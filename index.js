const toggleBtn = document.querySelector('.toggle-nav');
const sidebarOverlay = document.querySelector('.sidebar-overlay');
const sidebarCloseBtn = document.querySelector('.sidebar-close');

// console.log(closeBtn, sidebarOverlay, toggleBtn);
toggleBtn.addEventListener('click', ()=>{
  sidebarOverlay.classList.add('show');
})
sidebarCloseBtn.addEventListener('click', ()=>{
  sidebarOverlay.classList.remove('show');
})
